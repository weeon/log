module github.com/weeon/log

go 1.12

require (
	github.com/go-redis/redis/v8 v8.5.0
	github.com/nats-io/nats-server/v2 v2.3.4 // indirect
	github.com/nats-io/nats.go v1.11.1-0.20210623165838-4b75fc59ae30
	github.com/weeon/contract v0.0.0-20190520152601-a4ee53bdb563
	go.uber.org/zap v1.15.0
	gopkg.in/check.v1 v1.0.0-20190902080502-41f04d3bba15 // indirect
	gopkg.in/natefinch/lumberjack.v2 v2.0.0
)
